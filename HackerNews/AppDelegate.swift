//
//  AppDelegate.swift
//  HackerNews
//
//  Created by Alex Timonov on 04.04.16.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  let GlobalTintColor = UIColor(red: 1.0, green: 0.4, blue: 0.0, alpha: 1.0)
  
  var window: UIWindow?
  
  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
    configureUI()
    return true
  }
  
  func configureUI() {
    window?.tintColor = GlobalTintColor
  }
}
