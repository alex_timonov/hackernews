//
//  MainViewController.swift
//  HackerNews
//
//  Created by Alex Timonov on 04.04.16.
//

import UIKit
import SafariServices

class MainViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, SFSafariViewControllerDelegate {
  
  struct Story {
    let title: String
    let url: String?
    let by: String
    let score: Int
  }
  
  var firebase: Firebase!
  var stories: [Story]!
  var retrievingStories: Bool!
  var refreshControl: UIRefreshControl!
  var errorMessageLabel: UILabel!
  
  @IBOutlet weak var tableView: UITableView!
  
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    firebase = Firebase(url: "https://hacker-news.firebaseio.com/v0/")
    stories = []
    retrievingStories = false
    refreshControl = UIRefreshControl()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    configureUI()
    retrieveStories()
  }

  func configureUI() {
    refreshControl.addTarget(self, action: #selector(MainViewController.retrieveStories), forControlEvents: .ValueChanged)
    refreshControl.attributedTitle = NSAttributedString(string: "Refreshing...")
    tableView.insertSubview(refreshControl, atIndex: 0)

    errorMessageLabel = UILabel(frame: CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height))
    errorMessageLabel.textColor = UIColor.grayColor()
    errorMessageLabel.textAlignment = .Center
    errorMessageLabel.font = UIFont.systemFontOfSize(16)
  }
  
  func retrieveStories() {
    if retrievingStories! {
      return
    }
    
    UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    stories = []
    retrievingStories = true
    var storiesMap = [Int:Story]()
    
    let query = firebase.childByAppendingPath("topstories").queryLimitedToFirst(300)
    query.observeSingleEventOfType(.Value, withBlock: { snapshot in
      let storyIds = snapshot.value as! [Int]
      
      for storyId in storyIds {
        let query = self.firebase.childByAppendingPath("item").childByAppendingPath(String(storyId))
        query.observeSingleEventOfType(.Value, withBlock: { snapshot in
          storiesMap[storyId] = self.extractStory(snapshot)
          
          if storiesMap.count == Int(300) {
            var sortedStories = [Story]()
            for storyId in storyIds {
              sortedStories.append(storiesMap[storyId]!)
            }
            self.stories = sortedStories
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
            self.retrievingStories = false
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
          }
          }, withCancelBlock: self.loadingFailed)
      }
      }, withCancelBlock: self.loadingFailed)
  }
  
  private func extractStory(snapshot: FDataSnapshot) -> Story {
    let title = snapshot.value["title"] as! String
    let url = snapshot.value["url"] as? String
    let by = snapshot.value["by"] as! String
    let score = snapshot.value["score"] as! Int
    
    return Story(title: title, url: url, by: by, score: score)
  }
  
  private func loadingFailed(error: NSError!) -> Void {
    self.retrievingStories = false
    self.stories.removeAll()
    self.tableView.reloadData()
    self.showErrorMessage("Loading Failed!")
    UIApplication.sharedApplication().networkActivityIndicatorVisible = false
  }
  
  func showErrorMessage(message: String) {
    errorMessageLabel.text = message
    self.tableView.backgroundView = errorMessageLabel
    self.tableView.separatorStyle = .None
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return stories.count
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
  {
    let story = stories[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier("PostCell") as UITableViewCell!
    cell.textLabel?.text = story.title
    cell.detailTextLabel?.text = "\(story.score) points by \(story.by)"
    return cell
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    
    let story = stories[indexPath.row]
    if let url = story.url {
      let webViewController = SFSafariViewController(URL: NSURL(string: url)!)
      webViewController.delegate = self
      presentViewController(webViewController, animated: true, completion: nil)
    }
  }
  
  func safariViewControllerDidFinish(controller: SFSafariViewController) {
    controller.dismissViewControllerAnimated(true, completion: nil)
  }

}
